#!/usr/bin/env python3
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os

from xaa.loaders.util import boreas_file_to_dataframes

from xaa.core import SingleMeasurementProcessor
from xaa.plotting import CheckpointPlotter
from xaa.operations import Normalize, NormalizePeak, CheckPoint, Integrate, \
    SplitBy, Average, CombineDifference, Cut, CombineAverage, Flip, ApplyFunctionToY, BackToNamed

from xaa.operations import LineBGRemove, FermiBG, FermiWallAdaptive, ArctanAdaptiveRemove, FermiWallAdaptive2, FermiBGfittedA
from xaa.helpers import closest_idx
from xaa.xmcd_sumrules import sz_sumrule_l23, lz_sumrule_l23


labels_name = ['LAO', 'NGO', 'LSAT', 'LGO', 'STO', '3uc', '6uc', '10uc', '21uc']
labels_id = ['f1', 'f2', 'f3', 'f5', 'f4', 'b2', 'b1', 'b3', 'f6']
labels_dict = dict(zip(labels_id, labels_name))

table_xmcd_ni_high_b = [
    [[308, 316], 'f1', 2, 5],
    [[317, 325], 'f2', 2, 5],
    [[326, 334], 'f3', 2, 5],
    [[344, 352], 'f5', 2, 5],
    [[335, 343], 'f4', 2, 5],
    [[371, 379], 'b2', 2, 5],
    [[362, 370], 'b1', 2, 5],
    [[224, 232], 'b3', 2, 5],
    [[353, 361], 'f6', 2, 5]]

table_xmcd_mn_high_b = [
    [[236, 244], 'f1', 2, 5],
    [[245, 253], 'f2', 2, 5],
    [[254, 262], 'f3', 2, 5],
    [[272, 280], 'f5', 2, 5],
    [[263, 271], 'f4', 2, 5],
    [[299, 307], 'b2', 2, 5],
    [[290, 298], 'b1', 2, 5],
    [[206, 214], 'b3', 2, 5],
    [[281, 289], 'f6', 2, 5]]

cp_plotter = CheckpointPlotter()

filter_circular = lambda df: df.polarization[0] < 0

# working on this
pipeline_basic_TEY_xmcd = [Cut, SplitBy(binary_filter=filter_circular), Average, CheckPoint('raw'), LineBGRemove, CheckPoint('line'),
                           FermiWallAdaptive, CheckPoint('left_right'),
                           CombineAverage, Normalize(save='mu0'), CheckPoint('normalized_xas'),
                           Integrate, CheckPoint('integral_xas'),
                           BackToNamed('left_right'), Normalize(to='mu0'), CombineDifference, CheckPoint('xmcd'),
                           Integrate, CheckPoint('integral_xmcd')]

pipeline_basic_TEY_xmcd_LGO = [Cut, SplitBy(binary_filter=filter_circular), Average, ArctanAdaptiveRemove, LineBGRemove,
                               CheckPoint('line'),
                               FermiWallAdaptive2, CheckPoint('left_right'),
                               CombineAverage, Normalize(save='mu0'), CheckPoint('normalized_xas'),
                               Integrate, CheckPoint('integral_xas'),
                               BackToNamed('left_right'), Normalize(to='mu0'), CombineDifference, CheckPoint('xmcd'),
                               Integrate, CheckPoint('integral_xmcd')]

pipeline_basic_TFY_xmcd = [SplitBy(filter_circular), Average, ApplyFunctionToY(function=lambda y: 1 / y), LineBGRemove,
                           FermiBG, Flip, CheckPoint('left_right'),
                           CombineAverage, Normalize(save='mu0'), CheckPoint('normalized_xas'),
                           Integrate, CheckPoint('integral_xas'),
                           BackToNamed('left_right'), Normalize(to='mu0'), CombineDifference, CheckPoint('xmcd'),
                           Integrate, CheckPoint('integral_xmcd')]


def process_measurement(measurements_range, pipeline, pipeline_params, y_column, skip=[]):
    dataframes = boreas_file_to_dataframes('../data_files/SH1_Tue09.dat', measurements_range)

    # skip unwanted measurements
    cleaned_dfs = []
    for i in range(len(dataframes)):
        if i not in skip:
            cleaned_dfs.append(dataframes[i])
    dataframes = cleaned_dfs

    # feed the pipline, parameters and the data
    # then run the pipline
    p = SingleMeasurementProcessor()
    p.add_pipeline(pipeline)
    p.add_params(pipeline_params)
    p.check_missing_params()
    p.add_data(dataframes, x_column='energy', y_column=y_column)
    p.run()

    return p


def process_measurement_manual(measurements_range, pipeline, pipeline_params, y_column):
    dataframes = boreas_file_to_dataframes('../data_files/SH1_Tue09.dat', measurements_range)

    p = SingleMeasurementProcessor()
    p.add_pipeline(pipeline)

    p.add_params(pipeline_params)
    p.check_missing_params()
    p.add_data(dataframes, x_column='energy', y_column=y_column)
    p.run()
    return p


def thole_cara_lz(rho, n, l, c):
    # l=2 2*6*(10-n) / (6-2 +2) = 12/6 (10-n) rho = 2/3 (int + )
    return rho * 2 * (l * (l + 1) * (4 * l + 2 - n)) / (l * (l + 1) - c * (c + 1) + 2)


def thole_cara_sz(int1, int2, int_plus, n, l, c):
    # neglect Tz
    delta = (int2 - (2 * (c + 1) + 1) * (2 / c + 1) * int1) / (3 * int_plus)
    return delta * 3 * c * (4 * l + 2 - n) / (l * (l + 1) - 2 - c * (c + 1))


def calculate_sum_rules(processor, n, l, c_tc, n_3d, l3index, l2index, pre, mid, post):
    # a, b = l3index
    # c, d = l2index

    a, b = pre, mid
    c, d = mid, post

    int_xmcd = processor.get_checkpoint('integral_xmcd')[1]
    int_xas = processor.get_checkpoint('integral_xas')[1]

    int_l3 = int_xmcd[b] - int_xmcd[a]
    int_l2 = int_xmcd[d] - int_xmcd[c]
    int_xas_l3 = int_xas[b] - int_xas[a]
    int_xas_l2 = int_xas[d] - int_xas[c]
    total_xas = int_xas_l2 + int_xas_l3

    # sum rules by thole and cara lz
    rho = (int_l2 + int_l3) / (3 * (total_xas))
    # rho = int_xmcd[-1]/(3*int_xas[-1])
    lz = thole_cara_lz(rho=rho, n=n, l=2, c=c_tc)

    # sum rules by thole and cara sz
    sz = thole_cara_sz(int1=int_l2, int2=int_l3, int_plus=total_xas, n=n, l=2, c=c_tc)

    def guo_gupta_lz(total_xmcd, total_xas, n_3d):
        return -4 / 3 * total_xmcd / total_xas * (10 - n_3d)

    def guo_gupta_sz(int_l2, int_l3, total_xas, n_3d):
        return -1 * (2 * int_l3 - 4 * int_l2) / total_xas * (10 - n_3d) / 2

    # sum rule guo gupta
    sz_g = guo_gupta_sz(int_l2, int_l3, total_xas, n_3d)
    lz_g = guo_gupta_lz(int_l2 + int_l3, total_xas, n_3d)

    return lz, sz, lz_g, sz_g


def print_sumrules_thick(lz, sz, lzg, szg):
    print('\nthole cara sum rules')
    print("Lz 30 uc, 3 uc, 6 uc, 10 uc, 21 uc", lz)
    print("Sz 30 uc, 3 uc, 6 uc, 10 uc, 21 uc", sz)
    print('\nguo gupta reference')
    print("Lz 30 uc, 3 uc, 6 uc, 10 uc, 21 uc", lzg)
    print("Sz 30 uc, 3 uc, 6 uc, 10 uc, 21 uc", szg)


def print_sumrules_strain(lz, sz, lzg, szg):
    print('\nthole cara sum rules')
    print("Lz 'LAO', 'NGO', 'LSAT', 'LGO', 'STO'", lz)
    print("Sz 'LAO', 'NGO', 'LSAT', 'LGO', 'STO'", sz)
    print('\nguo gupta reference')
    print("Lz 'LAO', 'NGO', 'LSAT', 'LGO', 'STO'", lzg)
    print("Sz 'LAO', 'NGO', 'LSAT', 'LGO', 'STO'", szg)
    print("                                     ", np.array(lzg) / (np.array(szg)))


index_strain = ['LAO', 'NGO', 'LSAT', 'LGO', 'STO']
index_thick = [30, 3, 6, 10, 21]  # uc


def save_sumrules_csv(lz, sz, lzg, szg, fname, index):
    data = np.array([lz, sz, lzg, szg]).T
    df = pd.DataFrame(data, columns=['Lz', 'Sz', 'Lz_guo', 'Sz_guo'], index=index)
    df.to_csv('../out/compare_data' + os.sep + fname + '.csv')


def ni_xmcd_thickness():
    # ni xmcd thickness dependence

    ni_thick_params = {
        'cut_range': [835, 900],
        'line_range': [835, 845],
        'binary_filter': filter_circular,
        'peak_1': (853, 854.5),
        'peak_2': (871.48, 871.51),
        'mid': (857.5, 868),  # (859, 867),
        'post': (875, 884.5),
        'delta': 1.2}

    n = 2
    l = 2
    c = 1
    n_3d = 8.2  # cluster model analysis reference 27 guo gupta

    l2 = 868, 875
    l3 = 850, 861

    lz = []
    sz = []
    lzg = []
    szg = []

    for (a, b), label, _, _ in table_xmcd_ni_high_b[-5:]:
        measurements_range = range(a + 1, b + 1)

        p = process_measurement(measurements_range, pipeline_basic_TEY_xmcd, ni_thick_params, 'mu_normalized')

        df = p.df_from_named()
        df.to_csv('../out/ni_thick/{}.csv'.format(label))

        x = p.get_checkpoint('xmcd')[0]
        index_l2 = closest_idx(x, l2[0]), closest_idx(x, l2[1])
        index_l3 = closest_idx(x, l3[0]), closest_idx(x, l3[1])


        # sum rule calculations
        x = p.get_checkpoint('xmcd')[0]
        mid = closest_idx(x, 864)
        cut = closest_idx(x, ni_thick_params['line_range'][1])

        int_xas = p.get_checkpoint('integral_xas')[1]
        int_xmcd = p.get_checkpoint('integral_xmcd')[1]

        l3s = int_xmcd[mid] - int_xmcd[cut]
        l2s = int_xmcd[-1] - int_xmcd[mid]
        l3_xas = int_xas[mid] - int_xas[cut]
        l2_xas = int_xas[-1] - int_xas[mid]

        N = 3 * l3_xas + 3 * l2_xas
        ss = sz_sumrule_l23(N, l3s, l2s)
        ls = lz_sumrule_l23(N, l3s, l2s)

        sz.append(ss)
        lz.append(ls)
        szg.append(ss)
        lzg.append(ls)
        print(label)

        # cp_plotter.plot(p)

    print_sumrules_thick(lz, sz, lzg, szg)
    save_sumrules_csv(lz, sz, lzg, szg, 'ni_xmcd_thick', index_thick)


def ni_xmcd_strain(delta=1.2):

    #### settings ####
    ni_strain_params = {'cut_range': [838, 900],
                        'line_range': [838, 845],
                        'binary_filter': filter_circular,
                        'peak_1': (853, 854.5),
                        'peak_2': (871.28, 871.81),
                        'mid': (857.5, 868), # (859, 867),
                        'post': (874.8, 884.5),
                        'delta': delta,
                        #'gauss_fit_range': [846, 852.4],
                        'atan_adapt_pre': [841.5, 847.9], # for LGO fix
                        'atan_adapt_post': [853.1, 853.5] # for LGO fix
                        }

    ni_strain_params['pre'] = ni_strain_params['line_range']

    l2 = 868, 875
    l3 = 850, 861

    n = 8.2
    l = 2
    c = 1
    n_3d = 8.2  # cluster model analysis reference 27 guo gupta

    lz = []
    sz = []
    lzg = []
    szg = []

    measurements = table_xmcd_ni_high_b[:5]

    for i in range(0, len(measurements)):
        (a, b), label, _, _ = measurements[i]

        measurements_range = range(a + 1, b + 1)
        pipeline = pipeline_basic_TEY_xmcd

        # LGO normalization fix remove big peak at 851.5 eV with an atan baseline
        if label == 'f5':
            pipeline = pipeline_basic_TEY_xmcd_LGO

        p = process_measurement(measurements_range, pipeline, ni_strain_params, 'mu_normalized', skip=[])

        # save all in a csv
        df = p.df_from_named()
        df.to_csv('../out/ni_strain/{}.csv'.format(label))


        # sum rule calculations
        x = p.get_checkpoint('xmcd')[0]
        index_l2 = closest_idx(x, l2[0]), closest_idx(x, l2[1])
        index_l3 = closest_idx(x, l3[0]), closest_idx(x, l3[1])
        pre = closest_idx(x, 850)
        mid = closest_idx(x, 864)
        post = closest_idx(x, 876)
        cut = closest_idx(x, ni_strain_params['line_range'][1])

        int_xas = p.get_checkpoint('integral_xas')[1]
        int_xmcd = p.get_checkpoint('integral_xmcd')[1]

        l3s = int_xmcd[mid] - int_xmcd[cut]
        l2s = int_xmcd[-1] - int_xmcd[mid]
        l3_xas = int_xas[mid] - int_xas[cut]
        l2_xas = int_xas[-1] - int_xas[mid]

        N = 3 * l3_xas + 3 * l2_xas
        ss = sz_sumrule_l23(N, l3s, l2s)
        ls = lz_sumrule_l23(N, l3s, l2s)

        Lz, Sz, Lz_guo, Sz_guo = calculate_sum_rules(processor=p, n=n, l=l, c_tc=c, n_3d=n_3d, l2index=index_l2,
                                                     l3index=index_l3, pre=pre, mid=mid, post=post)
        sz.append(ss)  # Sz)
        lz.append(ls)  # Lz)
        szg.append(Sz_guo)
        lzg.append(Lz_guo)
        print(label)

        # plotter = CheckpointPlotter(['xmcd', 'normalized_xas', 'integral_xas', 'integral_xmcd'])
        # plotter.plot(p)

    print_sumrules_strain(lz, sz, lzg, szg)
    save_sumrules_csv(lz, sz, lzg, szg, 'ni_xmcd_strain', index_strain)
    return sz, lz


def mn_xmcd_thickness(delta=1.2):
    #pipeline_TEY_xmcd = [Cut, SplitBy(binary_filter=filter_circular), Average, LineBGRemove, FermiBG,
    #                     CheckPoint('left_right'),
    #                     CombineAverage, Normalize(save='mu0'), CheckPoint('normalized_xas'),
    #                     Integrate, CheckPoint('integral_xas'),
    #                     BackToNamed('left_right'), Normalize(to='mu0'), CombineDifference, CheckPoint('xmcd'),
    #                     Integrate, CheckPoint('integral_xmcd')]

    mn_thick_params = {'cut_range': (627, 662),
                       'line_range': (627, 635),
                       'binary_filter': filter_circular,
                       'peak_1': (642, 647),
                       'peak_2': (652, 656),
                       'post': (660, 662),
                       'a': 0.05, 'delta': 1.5}

    n = 2
    l = 2
    c = 1
    mid_point = 649  # eV
    n_3d = 3.8  # guo gupta cluster analysis


    pipeline_basic_TEY_xmcd = [Cut, SplitBy(binary_filter=filter_circular), Average,
                               LineBGRemove, CheckPoint('line'),
                               FermiBGfittedA, CheckPoint('left_right'),
                               CombineAverage, NormalizePeak(save='mu0'), CheckPoint('normalized_xas'),
                               Integrate, CheckPoint('integral_xas'),
                               BackToNamed('left_right'), Normalize(to='mu0'), CombineDifference, CheckPoint('xmcd'),
                               Integrate, CheckPoint('integral_xmcd')]


    #### settings ####
    mn_settings = {'cut_range': (627, 662),
                    'line_range': (627, 637),
                    'binary_filter': filter_circular,
                    'peak_1': (642, 643),
                    'peak_2': (652, 656),
                    'mid': (648, 649),
                    'post': (658, 662),
                    'delta': delta,
                    }

    mn_settings['pre'] = mn_settings['line_range']
    mn_settings['normalizepeak_range'] = (642.3, 642.7)

    l2 = 640, 647
    l3 = 651, 657

    l = 2
    c = 1
    n_3d = 3.8  # cluster model analysis reference 27 guo gupta



    lz = []
    sz = []
    lzg = []
    szg = []

    measurements = table_xmcd_mn_high_b[-5:]

    for i in range(0, len(measurements)):
        (start, end), label, _, _ = measurements[i]
        measurements_range = range(start + 1, end + 1)

        p = process_measurement(measurements_range, pipeline_basic_TEY_xmcd, mn_settings, 'mu_normalized')

        df = p.df_from_named()
        df.to_csv('../out/mn_thick/{}.csv'.format(label))

        mid = closest_idx(p.get_checkpoint('xmcd')[0], mid_point)  # int(22 / 35 * (len(p.get_checkpoint(3)[1])))
        #Lz, Sz, Lz_guo, Sz_guo = calculate_sum_rules(processor=p, mid=mid, n=n, l=l, c_tc=c, n_3d=n_3d)

        x = p.get_checkpoint('xmcd')[0]
        mid = closest_idx(x, mid_point)
        cut = closest_idx(x, mn_settings['line_range'][1])

        int_xas = p.get_checkpoint('integral_xas')[1]
        int_xmcd = p.get_checkpoint('integral_xmcd')[1]
        l3s = int_xmcd[mid] - int_xmcd[cut]
        l2s = int_xmcd[-1] - int_xmcd[mid]
        l3_xas = int_xas[mid] - int_xas[cut]
        l2_xas = int_xas[-1] - int_xas[mid]

        N = 3 * l3_xas + 3 * l2_xas
        ss = 1.47*sz_sumrule_l23(N, l3s, l2s, n_filled=n_3d)
        ls = 1.47*lz_sumrule_l23(N, l3s, l2s, n_filled=n_3d)

        sz.append(ss)
        lz.append(ls)
        #sz.append(Sz)
        #lz.append(Lz)
        szg.append(ss)
        lzg.append(ls)
        #szg.append(Sz_guo)
        #lzg.append(Lz_guo)

        print(label)

        #cp_plotter.plot(p)

    print_sumrules_thick(lz, sz, lzg, szg)
    save_sumrules_csv(lz, sz, lzg, szg, 'mn_xmcd_thick', index_thick)
    return sz, lz


def mn_xmcd_strain(delta=1.2):

    pipeline_basic_TEY_xmcd = [Cut, SplitBy(binary_filter=filter_circular), Average,
                               LineBGRemove, CheckPoint('line'),
                               FermiBGfittedA, CheckPoint('left_right'),
                               CombineAverage, Normalize(save='mu0'), CheckPoint('normalized_xas'),
                               Integrate, CheckPoint('integral_xas'),
                               BackToNamed('left_right'), Normalize(to='mu0'), CombineDifference, CheckPoint('xmcd'),
                               Integrate, CheckPoint('integral_xmcd')]


    #### settings ####
    mn_settings = {'cut_range': (627, 662),
                    'line_range': (627, 637),
                    'binary_filter': filter_circular,
                    'peak_1': (642, 643),
                    'peak_2': (652, 656),
                    'mid': (648, 649),
                    'post': (658, 662),
                    'delta': delta,
                    }

    mn_settings['pre'] = mn_settings['line_range']

    l2 = 640, 647
    l3 = 651, 657

    l = 2
    c = 1
    n_3d = 3.8  # cluster model analysis reference 27 guo gupta

    lz = []
    sz = []
    lzg = []
    szg = []

    measurements = table_xmcd_mn_high_b[:5]

    for i in range(0, len(measurements)):
        (a, b), label, _, _ = measurements[i]

        measurements_range = range(a + 1, b + 1)
        pipeline = pipeline_basic_TEY_xmcd

        p = process_measurement(measurements_range, pipeline, mn_settings, 'mu_normalized', skip=[])

        # save all in a csv
        df = p.df_from_named()
        df.to_csv('../out/mn_strain/{}.csv'.format(label))

        # sum rule calculations
        x = p.get_checkpoint('xmcd')[0]
        pre = closest_idx(x, 636)
        mid = closest_idx(x, 649)
        post = closest_idx(x, 660)
        cut = closest_idx(x, mn_settings['line_range'][1])

        int_xas = p.get_checkpoint('integral_xas')[1]
        int_xmcd = p.get_checkpoint('integral_xmcd')[1]

        l3s = int_xmcd[mid] - int_xmcd[cut]
        l2s = int_xmcd[-1] - int_xmcd[mid]
        l3_xas = int_xas[mid] - int_xas[cut]
        l2_xas = int_xas[-1] - int_xas[mid]

        N = 3 * l3_xas + 3 * l2_xas
        ss = sz_sumrule_l23(N, l3s, l2s, n_filled=n_3d)
        ls = lz_sumrule_l23(N, l3s, l2s, n_filled=n_3d)

        sz.append(ss*1.47)
        lz.append(ls*1.47)
        print(label)

        #plotter = CheckpointPlotter(['xmcd', 'normalized_xas', 'integral_xas', 'integral_xmcd'])
        #plotter.plot(p)

    print_sumrules_strain(lz, sz, lz, sz)
    save_sumrules_csv(lz, sz, lz, sz, 'mn_xmcd_strain', index_strain)
    return sz, lz


def crit_exp_fit():
    pipeline_basic_TEY_xmcd = [Cut, SplitBy(binary_filter=filter_circular), Average,
                               LineBGRemove, CheckPoint('line'),
                               FermiBGfittedA, CheckPoint('left_right'),
                               CombineAverage, Normalize(save='mu0'), CheckPoint('normalized_xas'),
                               Integrate, CheckPoint('integral_xas'),
                               BackToNamed('left_right'), Normalize(to='mu0'), CombineDifference,
                               CheckPoint('xmcd'),
                               Integrate, CheckPoint('integral_xmcd')]

    #### settings ####
    mn_settings = {'cut_range': (627, 662),
                   'line_range': (627, 637),
                   'binary_filter': filter_circular,
                   'peak_1': (642, 643),
                   'peak_2': (652, 656),
                   'mid': (648, 649),
                   'post': (658, 662),
                   'delta': 1.2,
                   }

    mn_settings['pre'] = mn_settings['line_range']
    n_3d = 3.8  # cluster model analysis reference 27 guo gupta


    # measurement, temp, mag field
    measurements = [
     [[497,501], 2, 5],
     [[502,506], 2, 2],
     [[509,513], 100, 5],
     [[514,518], 100, 2],
     [[520,524], 140, 5],
     [[525,529], 140, 2],
     [[530,534], 180, 5],
     [[535,539], 180, 2],
     [[540,544], 250, 5],
     [[545,549], 250, 2],
    ]

    temps = []
    mag_moment = []

    for (a, b), temp, field in measurements:
        if field == 5:
            continue
        a, b = a, b+1
        measurements_range = range(a, b)
        p = process_measurement(measurements_range, pipeline_basic_TEY_xmcd, mn_settings, 'mu_normalized', skip=[])


        # sum rule calculations
        x = p.get_checkpoint('xmcd')[0]
        mid = closest_idx(x, 649)
        cut = closest_idx(x, mn_settings['line_range'][1])

        int_xas = p.get_checkpoint('integral_xas')[1]
        int_xmcd = p.get_checkpoint('integral_xmcd')[1]

        l3s = int_xmcd[mid] - int_xmcd[cut]
        l2s = int_xmcd[-1] - int_xmcd[mid]
        l3_xas = int_xas[mid] - int_xas[cut]
        l2_xas = int_xas[-1] - int_xas[mid]

        N = 3 * l3_xas + 3 * l2_xas
        ss = sz_sumrule_l23(N, l3s, l2s, n_filled=n_3d)
        ls = lz_sumrule_l23(N, l3s, l2s, n_filled=n_3d)

        print(ss * 1.47)
        print(ls * 1.47)

        temps.append(temp)
        mag_moment.append(-2*ss*1.47-1.47*ls)

        #plotter = CheckpointPlotter(['xmcd', 'normalized_xas', 'integral_xas', 'integral_xmcd'])
        #plotter.plot(p)


    tc = 192
    def crit_exp(t, crit, a):
        return a*(tc-t)**crit

    from scipy.optimize import curve_fit

    popt, pcov = curve_fit(f=crit_exp, xdata=temps[:-1], ydata=mag_moment[:-1])
    print(popt, pcov)

    te = np.linspace(0, tc, 1000)
    te2 = [tc, 250]

    plt.figure()
    plt.plot(temps, mag_moment, 'x', label='$2 <S_z> + <L_z>$ ')
    plt.plot(te, crit_exp(te, *popt), 'g-', label='crit exponent fit $\\beta='+str(popt[0])+'$')
    plt.plot(te2, [0,0], 'g-')
    plt.xlabel('T [K]')
    plt.ylabel('[$\mu_b$]')
    plt.legend()
    plt.tight_layout()
    plt.show()

def main():
    ni_xmcd_thickness()
    ni_xmcd_strain()
    mn_xmcd_thickness()
    mn_xmcd_strain()
    crit_exp_fit()

if __name__ == '__main__':
    main()
