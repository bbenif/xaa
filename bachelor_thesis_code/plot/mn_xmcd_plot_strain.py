import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from plot_templates import XMCDPlot, IntegralPlotXMCD
from bachelor_thesis_examples.xmcd_main import mn_xmcd_strain

csv_dir = '../out/mn_strain/'
savedir = '../../../bachelor-thesis/figures/'
appendix_savedir = savedir + 'appendix_plots/'


# labels_name = ['LAO', 'NGO', 'LSAT', 'LGO', 'STO', '3uc', '6uc', '10uc', '21uc']
# labels_id = ['f1', 'f2', 'f3', 'f5', 'f4', 'b2', 'b1', 'b3', 'f6']
sample_ids_strain = ['f1', 'f2', 'f3', 'f5', 'f4']
labels_strain = ['LAO', 'NGO', 'LSAT', 'LGO', 'STO']
strain = [-2.190, -0.019, 0.188, 0.342, 1.133]


#ss = np.zeros((40,5))
#ls = np.zeros((40,5))

#for i, d in enumerate(np.linspace(0.5, 1.5, 40)):
#    s, l = ni_xmcd_strain(d)
#    ss[i,:] = s
#    ls[i,:] = l

#print(ss)
#print(ls)

s, l = mn_xmcd_strain()

#s_min, s_max = np.min(ss, axis=0), np.max(ss, axis=0)
#l_min, l_max = np.min(ls, axis=0), np.max(ls, axis=0)

#plt.figure()
#plt.errorbar(strain, -np.array(s), yerr=2*np.array([s-s_min, s_max-s]), fmt='.')
#plt.errorbar(strain, -np.array(l), yerr=5*np.array([l-l_min, l_max-l]), fmt='.')
#plt.show()

dfs = []
for sid in sample_ids_strain:
    dfs.append(pd.read_csv(csv_dir + '{}.csv'.format(sid), index_col=0))

def main():
    plot = XMCDPlot('Photon Energy [eV]', 'normalized xmcd (a.u.)', n_colors=5, inset_axis_dimensions=[0.65,0.62,0.3,0.33])
    for i in range(len(sample_ids_strain)):
        plot.plot(dfs[i].index, dfs[i]['xmcd'], labels_strain[i], i)
        plot.plot_inset([strain[i]], dfs[i]['integral_xmcd'].iloc[-1], i)
    plot.inset_axis_naming('strain [\\%]', 'integral xmcd (a.u.)')
    plot.finish(save=savedir + 'mn_strain_xmcd.pdf')

def mag_moments():
    plt.figure()
    plt.plot(strain, -np.array(s), '.', label='$<S_z>$')
    plt.plot(strain, -np.array(l), '.', label='$<L_z>$')
    plt.ylim((0,2.2))
    plt.xlabel('Strain [\\%]')
    plt.ylabel('Magnetic moment [$\mu_B$]')
    plt.legend(loc=(0.12,0.3), frameon=True)
    plt.tight_layout()
    plt.show()

def side():
    for i in range(len(sample_ids_strain)):
        x = dfs[i].index
        plot = IntegralPlotXMCD('Photon Energy [eV]', 'normalized xmcd [arb.u.]',
                                figsize=(5.906/2, 1.9), n_colors=4)
        print('{} {} {}'.format(sample_ids_strain[i], strain[i], labels_strain[i]))
        plot.plot(x, dfs[i]['xmcd'], 'xmcd', 0)
        plot.plot(x, dfs[i]['normalized_xas'], 'xas', 1)
        plot.plot(x, dfs[i]['integral_xmcd'], 'integral xmcd', 2)
        plot.plot(x, dfs[i]['integral_xas'], 'integral xas', 3)
        plot.finish(save=appendix_savedir + 'mn_strain_xmcd' + str(i) + '.pdf')
        #plot.finish()

if __name__ == '__main__':
    main()
    mag_moments()
    side()